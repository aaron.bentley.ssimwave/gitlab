import { helpPagePath } from '~/helpers/help_page_helper';

export const GEO_INFO_URL = helpPagePath('administration/geo/index.md');
